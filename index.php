<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Les manuels libres</title>
    <link href="datas/dsfr/dsfr.min.css" rel="stylesheet">
    <link href="datas/dsfr/utility.main.css" rel="stylesheet">
    <link href="datas/bootstrap/perso.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#FFFFFF" />
    <link href="manifest.json" rel="manifest">
    <?php
    if ($_SERVER['SERVER_NAME'] != 'localhost') {
        ?>
        <!-- Matomo -->
        <script>
            var _paq = window._paq = window._paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            _paq.push(["setCookieDomain", "*.lesmanuelslibres.dane.ac-versailles.fr"]);
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function() {
                var u="//statistiques.dane.ac-versailles.fr/";
                _paq.push(['setTrackerUrl', u+'matomo.php']);
                _paq.push(['setSiteId', '8']);
                var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
            })();
        </script>
        <!-- End Matomo Code -->
        <?php
    }
    ?>
</head>
<body>
<header role="banner" class="fr-header">

    <div class="fr-header__body ">
        <div class="fr-container">
            <div class="fr-header__body-row">
                <div class="fr-header__brand fr-enlarge-link">
                    <div class="fr-header__brand-top">
                        <div class="fr-header__logo">
                            <p class="fr-logo">
                                Région Académique
                                <br>Île-de-France
                            </p>
                        </div>
                        <div class="fr-header__operator">
                            <img class="fr-responsive-img" style="max-width:5rem;" src="datas/img/logo.png" alt="[À MODIFIER - texte alternatif de l’image]" >
                            <!-- L’alternative de l’image (attribut alt) doit impérativement être renseignée et reprendre le texte visible dans l’image -->
                        </div>
                        <div class="fr-header__navbar">
                            <button class="fr-btn fr-icon-info-line" data-fr-opened="false" aria-controls="fr-modal-apropos">
                                Menu
                            </button>
                        </div>
                    </div>
                    <div class="fr-header__service">
                        <a href="./" title="Le manuel libre">
                            <p class="fr-header__service-title" id="headerTitle">
                                Les manuels libres
                            </p>
                        </a>
                        <p class="fr-header__service-tagline">Par et pour les enseignants, pour les élèves</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

</header>
<main class="fr-container fr-pt-6w" id="container">

    <h6>
        La Région Académique met à disposition de tous les professeurs et de tous les élèves des lycées généraux, technologiques et professionnels des ressources pédagogiques numériques, libres et gratuites, créées par des auteurs des différentes académies de Créteil-Paris-Versailles.    </h6>

    <hr>
    <div class="fr-grid-row fr-grid-row--gutters">

        <div class="fr-col-12 fr-col-md-4">
            <h1 class="fr-mb-0"><span id="compteur"></span><small> Manuel<span class="pluriel">s</span></small></h1>
            <h3>Disponible<span class="pluriel">s</span></h3>
        </div>
        <div class="fr-col-12 fr-col-md-4">
            <div class="fr-select-group">
                <label class="fr-label" for="selectNiveau">
                    Filtre par niveau
                </label>
                <select class="fr-select" id="selectNiveau" name="niveau" onchange="filtrer()">
                    <option value="">Sélectionner un niveau</option>
                    <option value="">Tous les niveaux</option>
                </select>
            </div>
        </div>
        <div class="fr-col-12 fr-col-md-4">
            <div class="fr-select-group">
                <label class="fr-label" for="selectDiscipline">
                    Filtrer par discipline
                </label>
                <select class="fr-select" id="selectDiscipline" name="discipline" onchange="filtrer()">
                    <option value="" >Sélectionner une discipline</option>
                    <option value="" >Toutes les disciplines</option>
                </select>
            </div>
        </div>

    </div>
    <div class="fr-grid-row fr-grid-row--gutters">


<?php
$niveaux = array();
$disciplines = array();

$folder= "./";
$compteur = 0;

function carte($file, &$niveaux, &$disciplines)
{
    $infos = fopen($file."/datas/infos.txt", "r");
    $ligne = 1;
    while (!feof($infos)) {
        $info[$ligne] = fgets($infos);
        $ligne++;
    }
    fclose($infos);

    $niveaux[$info[1]] = 1;
    $disciplines[$info[2]] = 1;

    echo ' <div class="fr-col-xl-3 fr-col-lg-4 fr-col-md-4 fr-col-sm-6 fr-col-12 manuel '. str_replace(array("\r\n", "\r", "\n", " "), '', $info[1]).' '. str_replace(array("\r\n", "\r", "\n", " "), '', $info[2]).'">
  <div class="fr-card fr-enlarge-link">
            <div class="fr-card__body">
                <div class="fr-card__content">
                    <h3 class="fr-card__title">
                        <a href="'.$file.'/">'.$info[3].'</a>
                    </h3>
                    <p class="fr-card__desc">'.$info[4].'</p>
                </div>
            </div>
            <div class="fr-card__header">
                <div class="fr-card__img">
                    <img class="fr-responsive-img" src="'.$file.'/logo.jpg" alt="Logo">
                    <!-- L’alternative de l’image (attribut alt) doit toujours être présente, sa valeur peut-être vide (image n’apportant pas de sens supplémentaire au contexte) ou non (porteuse de texte ou apportant du sens) selon votre contexte -->
                </div>
                <ul class="fr-badges-group">
                    <li>
                        <p class="fr-badge fr-badge--green-emeraude">'.$info[1].'</p>
                    </li>
                </ul>
            </div>
        </div>

      
    </div>';
}


if ($dir = opendir($folder)) {
    while (false !== ($file = readdir($dir))) {
        if ($file != 'sw.js' and $file != 'favicon.ico' and $file != 'manifest.json' and $file != '.' and $file != '..' and $file != "index.php" and $file != "README.md" and $file != ".git" and $file != ".gitignore"  and $file != "datas" and $file != ".idea" and $file != "generateur" and $file != "tmp1" and $file != "tmp" and $file != "tmp2") {
            $fichierindex = $file . "/index.html";
            if (file_exists($fichierindex)) {
                carte($file, $niveaux, $disciplines);
                $compteur++;
            }
        }
    }
}

?>

    </div>

    <h2 id="rien" class="fr-hidden fr-mt-3w">Aucun manuel trouvé pour votre recherche</h2>

</main>

<footer class="fr-footer fr-mt-6w" role="contentinfo" id="footer">
    <div class="fr-container">
        <div class="fr-footer__body">
            <div class="fr-footer__brand fr-enlarge-link">
                <a href="/" title="Retour à l’accueil du site - Nom de l’entité (ministère, secrétariat d‘état, gouvernement)">
                    <p class="fr-logo">
                        Région Académique
                        <br>Île-de-France
                    </p>
                </a>
            </div>
            <div class="fr-footer__content">
                <ul class="fr-footer__content-list">
                    <li class="fr-footer__content-item">
                        <a class="fr-footer__content-link" target="_blank" rel="noopener external" title="Site de l'académie de Créteil" href="https://www.ac-creteil.fr/">ac-creteil.fr</a>
                    </li>
                    <li class="fr-footer__content-item">
                        <a class="fr-footer__content-link" target="_blank" rel="noopener external" title="Site de l'académie de Paris" href="https://www.ac-paris.fr/">ac-paris.fr</a>
                    </li>
                    <li class="fr-footer__content-item">
                        <a class="fr-footer__content-link" target="_blank" rel="noopener external" title="Site de l'académie de Versailles" href="https://www.ac-versailles.fr/">ac-versailles.fr</a>
                    </li>
                </ul>
            </div>

        </div>
        <div class="fr-footer__partners">
            <div class="fr-footer__partners-logos">
                <div class="fr-footer__partners-main">
                    <a class="fr-footer__partners-link" href="https://www.iledefrance.fr/" target="_blank">
                        <img class="fr-footer__logo" style="height: 4rem" src="datas/img/idf.jpeg" alt="Logo région Ile de France" />
                        <!-- L’alternative de l’image (attribut alt) doit impérativement être renseignée et reprendre le texte visible dans l’image -->
                    </a>
                </div>
                <div class="fr-footer__partners-sub">
                    <ul>
                        <li>
                            <a class="fr-footer__partners-link" href="https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.fr" target="_blank">
                                <img class="fr-footer__logo" style="" src="datas/img/licence.png" alt="Voir la licence CC BY-NC-SA 4.0 - nouvelle fenêtre" />
                                <!-- L’alternative de l’image (attribut alt) doit impérativement être renseignée et reprendre le texte visible dans l’image -->
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="fr-footer__bottom">
            <ul class="fr-footer__bottom-list">
                <li class="fr-footer__bottom-item">
                    <a class="fr-footer__bottom-link" href="#">Accessibilité : partiellement conforme</a>
                </li>
                <li class="fr-footer__bottom-item">
                    <a class="fr-footer__bottom-link" href="#"  data-fr-opened="false" aria-controls="fr-modal-mentions">Mentions légales</a>
                </li>
            </ul>
            <div class="fr-footer__bottom-copy">
                <p>Sous réserve des droits de propriété intellectuelle de tiers, les contenus de ce site sont proposés dans le cadre du droit Français sous <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.fr" rel="noopener external" title="Voir la licence CC BY-NC-SA 4.0 - nouvelle fenêtre" target="_blank">licence CC BY-NC-SA 4.0</a>
                </p>
            </div>
        </div>
    </div>
</footer>


<dialog aria-labelledby="fr-modal-title-modal-4" role="dialog" id="fr-modal-mentions" class="fr-modal">
    <div class="fr-container fr-container--fluid fr-container-md">
        <div class="fr-grid-row fr-grid-row--center">
            <div class="fr-col-12 fr-col-md-8">
                <div class="fr-modal__body">
                    <div class="fr-modal__header">
                        <button class="fr-btn--close fr-btn" title="Fermer la fenêtre modale" aria-controls="fr-modal-mentions" target="_self">Fermer</button>
                    </div>
                    <div class="fr-modal__content">
                        <h1 id="fr-modal-title-modal-4" class="fr-modal__title"><span class="fr-icon-arrow-right-line fr-icon--lg"></span>Mentions légales</h1>
                        <div id="note-text">
                            <h2>Information générales</h2>

                            <p class="fr-text--bold">Les manuels libres </p>
                            <p>https://lesmanuelslibres.region-academique-idf.fr</p>
                            <p>Les Manuels Libres sont des manuels scolaires développés en partenariat entre la Région académique et la Région Île-de-France.</p>
                            <p><span class="fr-text--bold">Directeur de la publication : </span>Bernard BEIGNIER, Recteur de la région académique Île-de-France</p>
                            <p><span class="fr-text--bold">Suivi éditorial : </span>Pierre CAUTY - DRANE (Délégué Région Académique au Numérique Éducatif)</p>
                            <p><span class="fr-text--bold">Webmestre : </span><a href="https://www.dane.ac-versailles.fr/spip.php?auteur130" target="_blank">Philippe ROCA (DRANE Île-de-France - Site de Versailles)</a></p>
                            <p><span class="fr-text--bold">Hébergeur assurant le stockage direct et permanent :</span><br>
                                DRASI Rectorat de l'académie de Versailles - 3 Bd de Lesseps, 78017 Versailles
                            </p>

                            <h2 class="spip">Crédits et licence</h2>
                            <p>Sous réserve des droits de propriété intellectuelle de tiers, les contenus de ce site sont proposés dans le cadre du droit Français sous <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.fr" rel="noopener external" title="Voir la licence CC BY-NC-SA 4.0 - nouvelle fenêtre" target="_blank">licence CC BY-NC-SA 4.0</a>
                            <p>
                                <a class="fr-footer__partners-link" href="https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.fr" target="_blank">
                                    <img class="fr-footer__logo" style="" src="datas/img/licence.png" alt="Voir la licence CC BY-NC-SA 4.0 - nouvelle fenêtre" />
                                    <!-- L’alternative de l’image (attribut alt) doit impérativement être renseignée et reprendre le texte visible dans l’image -->
                                </a>
                            </p>

                            <p>Il n'est donc pas possible de les utiliser dans le cadre d'un usage commercial ou associés à un service ou une prestation commerciale. Pour toute demande d'information concernant un usage commercial, veuillez contacter : <a href="mailto:manuelsscolaires@iledefrance.fr">manuelsscolaires@iledefrance.fr</a></p>
<h2 class="spip">Contact</h2>
                            <p>Formulaire&nbsp;: <a href="https://www.dane.ac-versailles.fr/spip.php?page=contact">Page de contact</a></p>

                            <h2 class="spip">Données statistiques&nbsp;: traçabilité et profilage</h2>
                            <p>Le site <a href="https://lesmanuelslibres.region-academique-idf.fr" class='spip_url spip_out auto' rel='nofollow external'>https://lesmanuelslibres.region-academique-idf.fr/</a> respecte la vie privée de l’usager et se conforme strictement aux lois en vigueur sur la protection de la vie privée et des libertés individuelles. Aucune information personnelle n’est collectée à votre insu.</p>
                            <p>Ce site n'utilise pas de cookies</p>
                            <p>Ce site utilise le logiciel statistique Matomo qui est Open Source recommandé par la CNIL. Il est configuré suivant les recommandations de la CNIL.<br>
                                Afin d'analyser le nombre de visites, il collecte les adresses IP anonymes, les types de clients et appareils utilisés pour la consultation. Aucune donnée personnelle ou nominative est collectée.</p>

                            <h2>À propos de ce site</h2>
                            <ul>
                                <li>Développement et conception du moteur d'affichage : Philippe ROCA (DRANE Île-de-France - Site de Versailles)</li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</dialog>


</body>
<script type="module" src="datas/dsfr/dsfr.module.min.js"></script>
<script  src="datas/js/app.js" defer></script>

<script>
    document.addEventListener("DOMContentLoaded", function() {
        var maBalise = document.getElementById("compteur");
        maBalise.textContent = "<?= $compteur; ?>";
        var selectNiveau = document.getElementById("selectNiveau");
        var selectDiscipline = document.getElementById("selectDiscipline");
        <?php
        $i = 0;
        foreach ($niveaux as $cle => $valeur) {
            $i++;
            $val = str_replace(array("\r\n", "\r", "\n"), '', $cle);
            $class = str_replace(array("\r\n", "\r", "\n", " "), '', $cle);
            echo 'var nouvelleOption'.$i.' = document.createElement("option"); ';
            echo 'nouvelleOption'.$i.'.text = "'.$val.'"; ';
            echo 'nouvelleOption'.$i.'.value = "'.$class.'"; ';
            echo 'selectNiveau.add(nouvelleOption'.$i.'); ';
        }
        foreach ($disciplines as $cle => $valeur) {
            $i++;
            $val = str_replace(array("\r\n", "\r", "\n"), '', $cle);
            $class = str_replace(array("\r\n", "\r", "\n", " "), '', $cle);
            echo 'var nouvelleOption'.$i.' = document.createElement("option"); ';
            echo 'nouvelleOption'.$i.'.text = "'.$val.'"; ';
            echo 'nouvelleOption'.$i.'.value = "'.$class.'"; ';
            echo 'selectDiscipline.add(nouvelleOption'.$i.'); ';
        }
        ?>
    });

    function filtrer(){
        var selectNiveau = document.getElementById("selectNiveau");
        var selectDiscipline = document.getElementById("selectDiscipline");
        var niveau = selectNiveau.value;
        var discipline = selectDiscipline.value;
        console.log("niveau sélectionné : " + niveau);
        console.log("discipline sélectionnée : " + discipline);
        var targetCacher = '.manuel';
        var targetMontrer = '.manuel';
        // if (niveau != ''){
        //     targetCacher = targetCacher + ':not(.' + niveau + ')';
        //     targetMontrer = targetMontrer + '.' + niveau;
        // }
        // if (discipline != ''){
        //     targetCacher = targetCacher + ':not(.' + discipline + ')';
        //     targetMontrer = targetMontrer + '.' + discipline;
        // }


        if (niveau != ''){
            targetCacher = targetCacher + ':not(.' + niveau;
            targetMontrer = targetMontrer + '.' + niveau;
            if (discipline != ''){
                targetCacher = targetCacher + '.' + discipline;
                targetMontrer = targetMontrer + '.' + discipline;
            }
            targetCacher = targetCacher + ')';
        }else{
            if (discipline != ''){
                targetCacher = targetCacher + ':not(.' + discipline + ')';
                targetMontrer = targetMontrer + '.' + discipline;
            }else{
                targetCacher = '.rien';
            }
        }


        console.log("cacher : " + targetCacher);
        console.log("montrer : " + targetMontrer);
        var manuelsCacher = document.querySelectorAll(targetCacher);
        var manuelsMontrer = document.querySelectorAll(targetMontrer);
        var pluriel =  document.querySelectorAll('.pluriel');
        var nbcache = 0;
        var total = <?= $compteur; ?>;
        var rien = document.getElementById("rien");
        manuelsCacher.forEach(function(div) {
            div.classList.add('fr-hidden')
            nbcache++;
        });
        manuelsMontrer.forEach(function(div) {
            div.classList.remove('fr-hidden')
        });
        if (total == nbcache)
        {
            rien.classList.remove("fr-hidden");
        }else{
            rien.classList.add("fr-hidden");
        }
        var dispo = total - nbcache;
        var maBalise = document.getElementById("compteur");
        maBalise.textContent = dispo;
        if (dispo <= 1)
        {
            pluriel.forEach(function(div) {
                div.classList.add('fr-hidden')
            });
        }else{
            pluriel.forEach(function(div) {
                div.classList.remove('fr-hidden')
            });
        }
    }
</script>
</html>

