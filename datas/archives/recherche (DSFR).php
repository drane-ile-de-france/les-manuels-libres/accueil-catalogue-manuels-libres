<?php
$folder = $_POST['chemin'];

$recherche = $_POST['recherche'];
$recherche = strtolower($recherche);
$recherche .= " ";
$recherche = str_replace('s ', ' ', $recherche);

    $actual_link = "$_SERVER[REQUEST_URI]";
    $url = str_replace("recherche.php", "", $actual_link).$folder."/";

$actual_link = str_replace('recherche.php', '', $actual_link);

$folder = str_replace('%20', ' ', $folder);
$folder = str_replace('%C3%A7', 'ç', $folder);
$folder = str_replace($actual_link, '', $folder);


function afficher($item,$folder, $compteur)
{

    $lien2 = str_replace('%20', ' ', $item->fichier);
    echo '<div class="fr-callout fr-p-1w">
            <h3 class="fr-callout__title">'.$item->titre.'</h3>
            <p class="fr-callout__text">'.$item->chemin.'</p>
            <a class="fr-btn fr-btn--sm ouvrirDepuisRecherche" href="'.$item->fichier.'" target="iframeAffichage" onclick="$(\'#boutonAfficherRecherche\').click(); curseurSommaire(\''.$lien2.'\'); ariane(\''.$lien2.'\', listePerles);">
                Voir la page
            </a>
        </div>';
    $compteur++;
    return $compteur;
}


$texte = file_get_contents($folder.'/liste.txt');
$articles = json_decode($texte);
$texte = file_get_contents($folder.'/mots.txt');
$mots = json_decode($texte);



$recherche = explode(" ", $recherche);
$pages = array();

foreach ($recherche as $mot)
{
    foreach($mots->$mot as $page=>$valeur) {
        $pages[$page] = $pages[$page] + $valeur;
    }
}


asort($pages);
$goodListe = array_reverse($pages, $preserve_keys = true);

$compteur = 0;
foreach($goodListe as $page=>$total) {
        $compteur = afficher($articles[$page], $folder, $compteur);

}
 if ($compteur == 0)
 {
     echo "<h6>Aucun résultat pour votre recherche</h6>";
 }


 ?>
<script>
   actuLiens();
</script>
