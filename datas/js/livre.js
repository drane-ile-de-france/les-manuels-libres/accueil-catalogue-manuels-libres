

    var adressePagePrecedente = document.referrer;
    if (adressePagePrecedente != '')
    {
        $('#boutonAccueil').removeClass('d-none');
    }

    // on activ les info bulles de bootstrap
    var tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
    var tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))


    // on surligne la page dans le sommaire qui est dans le modal
    function curseurSommaire(lien){
    $('#modalBody div.pearl').removeClass('active')
    $( "#modalBody div.pearl:has(a[href='"+lien+"'])" ).addClass('active')
    setTimeout(() => {
    var scrollTop = $( "#modalBody div.active" ).offset().top;
    $('#modalBody').scrollTop(scrollTop);

}, "1000")

}

    // on génère le fil d'ariane avec les boutons pages suivantes et précédente
    function ariane(lien, listePerles){
    $(".tooltip").fadeOut();
    var position = jQuery.inArray(lien, listePerles)
    var liste = $( "#modalBody div.pearl:has(a[href='"+lien+"'])" ).parents()
    .map(function() {
    var ligne =  this.getAttribute('class');
    if (ligne == 'sub-tree'){
    return $(this).find('.tree-title').first().text().replace('\n', '');
}
})
    .get();
    var suivant = ''
    var precedent = ''
    var fil = liste.reverse().join(' > ')
    if (position >= 1){
    precedent = "<div  data-bs-toggle='tooltip' data-bs-placement='bottom' data-bs-custom-class='custom-tooltip' data-bs-title='Page précedente'><a href='" + location.protocol + '//' + location.hostname + pathname + listePerles[position - 1] + "' target='iframeAffichage' onclick=' curseurSommaire(\""+listePerles[position - 1]+"\"); ariane(\""+listePerles[position - 1]+"\", listePerles);'><img src='../datas/img/gauche.png' class='page'></a></div>"
}
    if (position <= listePerles.length - 2){
    suivant = "<div  data-bs-toggle='tooltip' data-bs-placement='bottom' data-bs-custom-class='custom-tooltip' data-bs-title='Page suivante'><a href='" + location.protocol + '//' + location.hostname + pathname + listePerles[position + 1] + "'  target='iframeAffichage' onclick=' curseurSommaire(\""+listePerles[position + 1]+"\"); ariane(\""+listePerles[position + 1]+"\", listePerles);'><img src='../datas/img/droite.png' class='page' ></a></div>"
}
    setTimeout(() => {
    $('#ariane').text(fil)
    $('#avant').html(precedent)
    $('#apres').html(suivant)
    $('#iframeAffichage').contents().find('.central-column').css('width', '100%')
    $('#iframeAffichage').contents().find('.central-column').css('justify-content', 'left')

    $('#iframeAffichage').height($('#iframeAffichage').contents().find('body').height()+100)
    var tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
    var tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))

}, "200")

}


    // on supprimer les perles vides dans le modal sommaire
    function nettoyerPerlesSansTitre(element)
    {
        var liste = []
        $(element + ' a ').each(function() {
            // on supprimer les perles sans titre
            if ($(this).text() == "\nPerle sans titre") {
            $(this).remove()
            }else{
                liste.push($(this).attr('href'))
            }
        });
        // on supprime les balises div des perles sans titre
        $(element + ' div').each(function() {
            if ($(this).text() == '\n\n'){
                $(this).remove()
            }
        });
        // on supprimer le titre de la section si elle n'a plus de perle
        $(element + ' .section').each(function() {
            if (($(this).next().hasClass('section')))
            {
                $(this).remove()
            }
        });
        $(element + ' .children div:last-child ').each(function() {
            if (($(this).hasClass('section')))
            {
                $(this).remove()
            }
        });
        return liste
    }

    // on supprime les perles vides sur la page d'accueil
    function nettoyerIframe(){
    // on supprimer les perles sans titre
    $('#iframeAffichage').contents().find('a').each(function() {
        // on supprimer les perles sans titre
        if ($(this).text() == "\nPerle sans titre") {
            $(this).remove()
        }
    });
    // on supprime les balises div des perles sans titre
    $('#iframeAffichage').contents().find('div').each(function() {
        if ($(this).text() == '\n\n'){
            $(this).remove()
        }
    });

    // on supprimer le titre de la section si elle n'a plus de perle
    $('#iframeAffichage').contents().find('.section').each(function() {
        if (($(this).next().hasClass('section')))
        {
            $(this).remove()
        }
    });

    $('#iframeAffichage').contents().find('.children div:last-child ').each(function() {
        if (($(this).hasClass('section')))
        {
            $(this).remove()
        }

    });
}


    // la liste des perles est utilisée pour la pagination
    var listePerles = []

    const pathname = window.location.pathname;
    // header = $('header').height()
    // hauteur = $( window ).height() - header
    // $('#iframeAffichage').css('height',hauteur + 'px' )


    // on scanne le sommaire pour le mettre dans le modal et surtout mettre le target dans l'iframe
    $.get('Sommaire.html', function(html) {
        $('title').text($.parseHTML( html )[3]['innerText']);
        $('#titreModal').text($.parseHTML( html )[3]['innerText']);
        $('#headerTitle').text($.parseHTML( html )[3]['innerText']);
        contenu = $.parseHTML( html )[7]['innerHTML']
        children = $.parseHTML( contenu )[5]['innerHTML']
        children2 = $.parseHTML( children )
        $('#modalBody').html(children2);
        $('#modalBody a ').each(function() {
            $(this).attr('target', 'iframeAffichage')
            $(this).on("click", function (){
                lien = location.protocol + '//' + location.hostname + pathname + $(this).attr('href')
                $('#lienPartage').val(lien)
                codeiFrame = '<iframe width="900px" height="600px" src="'+lien+'"></iframe>'
                $('#codePartage').text(codeiFrame)
                $('#fermerModal').click()
            })
        });

        listePerles = nettoyerPerlesSansTitre('#modalBody')
        nettoyerIframe()



        // actualisation des données de partage de pages
        lien = location.protocol + '//' + location.hostname + pathname + 'Sommaire.html'
        $('#lienPartage').val(lien)
        codeiFrame = '<iframe width="900px" height="600px" src="'+lien+'"></iframe>'
        $('#codePartage').text(codeiFrame)
        $('#iframeAffichage').contents().find('a').each(function() {
            $(this).on("click", function (){
                lien = location.protocol + '//' + location.hostname + pathname + $(this).attr('href')
                codeiFrame = '<iframe width="900px" height="600px" src="'+lien+'"></iframe>'
                $('#lienPartage').val(lien)
                $('#codePartage').text(codeiFrame)
                lien = $(this).attr('href')
                curseurSommaire(lien)
                ariane(lien, listePerles)
            })
        });

        // au click sur le sommaire, on actualise le sommaire et le fil d'ariane
        $('#modalBody a').on("click", function (){
            lien = $(this).attr('href')
            curseurSommaire(lien)
            ariane(lien, listePerles)
        })


    });


    // fonction pour copier les codes d'intégration de page
    var toCopy  = document.getElementById( 'lienPartage' ),
    btnCopy = document.getElementById( 'copier' ),
    toCopy2  = document.getElementById( 'codePartage' ),
    btnCopy2 = document.getElementById( 'copier2' );

    btnCopy.addEventListener( 'click', function(){
    toCopy.select();
    document.execCommand( 'copy' );
    return false;
} );
    btnCopy2.addEventListener( 'click', function(){
    toCopy2.select();
    document.execCommand( 'copy' );
    return false;
} );




    function actuLiens(){
    $('#resultatRecherche a').on("click", function (){
        lien = location.protocol + '//' + location.hostname + pathname + $(this).attr('href')
        codeiFrame = '<iframe width="900px" height="600px" src="'+lien+'"></iframe>'
        $('#lienPartage').val(lien)
        $('#codePartage').text(codeiFrame)

    })
}
    setTimeout(() => {
    $('#iframeAffichage').height($('#iframeAffichage').contents().find('body').height()+100)
}, "100")




    function recherche(rechercher){
    var debut = new Date();

    $.getJSON('mots.txt', function(mots) {
    $.getJSON('liste.txt', function(liste) {
    var pages = {};

    // Traitement des données
    var recherche = rechercher.split(" ");
    $.each(recherche, function(index, mot) {
    $.each(mots[mot], function(page, valeur) {
    pages[page] = (pages[page] || 0) + valeur;
});
});

    // Tri des pages
    var sortedPages = Object.keys(pages).sort(function(a, b) {
    return pages[a] - pages[b];
});

    // Création de la liste triée
    var goodListe = {};
    sortedPages.reverse().forEach(function(page) {
    goodListe[page] = pages[page];
});

    $('#resultatRecherche').empty()
    $.each(goodListe, function(index, nb) {

    var ligne = afficher(liste[index])
    $('#resultatRecherche').append(ligne)
});
    var fin = new Date();
    var tempsEcoule = fin - debut;

    console.log("Temps écoulé : " + tempsEcoule + " millisecondes");
});
});
}

    function afficher(objet)
    {
        var lien = objet.fichier.replace(/%20/g, ' ');
        var ligne = '<a  class="ouvrirDepuisRecherche list-group-item list-group-item-action d-flex gap-3 py-3" href="' + objet.fichier + '" target="iframeAffichage" onclick="$(\'#boutonAfficherRecherche\').click(); curseurSommaire(\'' + lien + '\'); ariane(\'' + lien + '\', listePerles);" aria-current="true"><div class="d-flex gap-2 w-100 justify-content-between"><div><h6 class="mb-0">' + objet.titre + '</h6><p class="mb-0 opacity-75">' + objet.chemin + '</p></div><small class="text-primary text-nowrap">Voir la page</small></div></a>'
        return ligne
    }

    $( "#formSearch" ).on( "submit", function( event ) {
    $('#resultatRecherche').html('<img src="../datas/img/attente.gif">')
    var quoi = $('#search').val();
    $('#quoi').text(quoi)

    event.preventDefault();

    recherche(quoi)

});

