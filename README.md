# Page d'accueil Manuels Libres



## Installation

Sur la racine de l'hébergement, installer les fichiers et répertoires suivants :
- index.php
- recherchebs.php
- /datas
- /generateur

## Utilisation

il est impératif que les manuels installés dans les sous répertoires aient le fichier *datas/infos.txt*